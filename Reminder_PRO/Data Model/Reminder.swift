//
//  Reminder.swift
//  Reminder
//
//  Created by Boss on 15.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import Foundation
import RealmSwift

class Reminder: Object
{
    @objc dynamic var reminderID: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    @objc dynamic var reminder_date_time: Date?
    
    // Optional int property, defaulting to nil
    // RealmOptional properties should always be declared with `let`,
    // as assigning to them directly will not work as desired
    var repeat_interval = RealmOptional<Int>()
    var alarm_status    = RealmOptional<Int>()
    var priority        = RealmOptional<Int>()
    
    /*
     deprecated hatasi icin denedim ama alarm kurduktan sonra notification acinca hata veriyor
     var repeat_interval = RealmProperty<Int?>()
     var alarm_status    = RealmProperty<Int?>()
     var priority        = RealmProperty<Int?>()
     */
    
    
    @objc dynamic var done:Bool = false
    @objc dynamic var photo_path:String = ""
    @objc dynamic var recorded_sound_path:String = ""
    @objc dynamic var reminderColor:String = ""
    
    override static func primaryKey() -> String?
    {
        return "reminderID"
    }

}
 
