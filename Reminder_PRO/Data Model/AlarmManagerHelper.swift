//
//  AlarmManagerHelper.swift
//  Reminder
//
//  Created by Boss on 13.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class AlarmManagerHelper {
    static var app: AlarmManagerHelper = {
        return AlarmManagerHelper()
    }()
    
    ///App Controller singleton
    let appController = AppController.sharedInstance()
    var selectedIntervalInMillis :Int  = 0
    
    func scheduleNormalLocalNotification(reminderIdentifier:String, reminderDate:Date, title:String, body:String, for24Hours:Bool, badge:NSNumber)
    {
        // Create Notification Content
        let content = UNMutableNotificationContent()
        
        // Configure Notification Content
        content.title = title
        content.body = body
        content.subtitle = Helper.app.formatedDateAndTime(date: reminderDate, for24Hours: for24Hours)
        content.badge = badge
        content.categoryIdentifier = "normalReminder"
        content.userInfo = ["REMINDER_ID" : reminderIdentifier,
                            "ALARM_STATUS" : "normalAlarm"]
        
        appController.userDefaults.synchronize()
        if let notificationSoundDictionary = appController.userDefaults.dictionary(forKey: Helper.app.dictionaryNotificationSoundKey)
        {
            let fileName = notificationSoundDictionary["fileName"] as! String
            
            if (fileName != "")
            {
                content.sound = UNNotificationSound(named: UNNotificationSoundName(fileName))
            }
            else
            {
                content.sound = UNNotificationSound.default
            }
            //print(fileName)
        }
        else
        {
            content.sound = UNNotificationSound.default
        }
        
        var triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: reminderDate)
        triggerDate.second = 0
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: reminderIdentifier, content: content, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error
            {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                //Helper.app.showAlert(title: Helper.Alerts.NotificationsRequestFailed, message: String(describing: error.localizedDescription), vc: self)
            }
        }
    }
    
     func scheduleRepeatableLocalNotification(reminderIdentifier:String, reminderDate:Date, title:String, body:String, for24Hours:Bool, badge:NSNumber, repeatIntervalPosition: Int)
    {
        // Create Notification Content
        let content = UNMutableNotificationContent()
        
        // Configure Notification Content
        content.title = title
        content.body = body
        content.subtitle = Helper.app.formatedDateAndTime(date: reminderDate, for24Hours: for24Hours)
        content.badge = badge
        content.categoryIdentifier = "repeatableReminder"
        content.userInfo = ["REMINDER_ID" : reminderIdentifier,
                            "ALARM_STATUS" : "repeatableAlarm"]
        
        appController.userDefaults.synchronize()
        if let notificationSoundDictionary = appController.userDefaults.dictionary(forKey: Helper.app.dictionaryNotificationSoundKey)
        {
            let fileName = notificationSoundDictionary["fileName"] as! String
            
            if (fileName != "")
            {
                content.sound = UNNotificationSound(named: UNNotificationSoundName(fileName))
            }
            else
            {
                content.sound = UNNotificationSound.default
            }
            //print(fileName)
        }
        else
        {
            content.sound = UNNotificationSound.default
        }
       
        let trigger : UNCalendarNotificationTrigger
        
        switch (repeatIntervalPosition)
        {
            case 0:
                //Never
                var triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: reminderDate)
                triggerDate.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
                break
            
            case 1:
                //Hourly
                var triggerHourly = Calendar.current.dateComponents([.minute, .second], from: reminderDate)
                triggerHourly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerHourly, repeats: true)
                break
            
            case 2:
                //Daily
                var triggerDaily = Calendar.current.dateComponents([.hour, .minute, .second], from: reminderDate)
                triggerDaily.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
                break
            
            case 3:
                //Weekly
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 4:
                //Montly
                var triggerMontly = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: reminderDate)
                triggerMontly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerMontly, repeats: true)
                break
            
            case 5:
                //Yearly --OK
                var triggerYearly = Calendar.current.dateComponents([.month, .day, .hour, .minute, .second], from: reminderDate)
                triggerYearly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerYearly, repeats: true)
                break
            
            case 6:
                //Sunday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 1
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 7:
                //Monday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 2
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 8:
                //Tuesday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 3
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 9:
                //Wednesday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 4
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 10:
                //Thursday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 5
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 11:
                //Friday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 6
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break
            
            case 12:
                //Saturday
                var triggerWeekly = Calendar.current.dateComponents([.weekday, .hour, .minute, .second], from: reminderDate)
                triggerWeekly.weekday = 7
                triggerWeekly.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
                break

            
            default:
                var triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: reminderDate)
                triggerDate.second = 0
                trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
                break
        }
        
    
        //let nextDate = trigger.nextTriggerDate()
        //print("nowwDate = \(String(describing: reminderDate))")
        //print("nextDate = \(String(describing: nextDate))")
        
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: reminderIdentifier, content: content, trigger: trigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error
            {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                //Helper.app.showAlert(title: Helper.Alerts.NotificationsRequestFailed, message: String(describing: error.localizedDescription), vc: self)
            }
        }
    }
    
    
    
    func cancelLocalNotification(reminderIdentifier:String)
    {
        var identifiers: [String] = []
        identifiers.append(reminderIdentifier)
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
    }
}
