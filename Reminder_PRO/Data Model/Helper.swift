//
//  Helper.swift
//  Reminder
//
//  Created by Boss on 5.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class Helper {
    static var app: Helper = {
        return Helper()
    }()
    
    let primaryColor      = UIColor.blue.withAlphaComponent(0.3)
    let secondaryColor    = UIColor.white.withAlphaComponent(0.7)
    let defaultStateColor = UIColor.white.withAlphaComponent(0.5)
    let placeHolderColor  = UIColor.white.withAlphaComponent(0.4)
    
    let titleTextAttribute = [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.4), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Thin", size: 26)!]
    
    let admobAppID           = "ca-app-pub-6015724926414985~5140020541"
    let bannerAdUnitID       = "ca-app-pub-6015724926414985/9167019176"
    let interstitialAdUnitId = "ca-app-pub-6015724926414985/6544725262"
    
    //let admobTESTAppID           = "ca-app-pub-3940256099942544~1458002511"
    //let bannerTESTAdUnitID       = "ca-app-pub-3940256099942544/2934735716"
    //let interstitialTESTAdUnitId = "ca-app-pub-3940256099942544/4411468910"
    
    let for24HoursKey = "for24Hours"
    let dictionaryNotificationSoundKey = "notificationSoundDictionary"
    let snoozeDurationKey = "snoozeDuration"
    let sortByDateKey = "sortByDate"
    let adCounterKey  = "adCounter"
    
    // MARK: Alerts
    
    struct Alerts {
        static let DismissAlert                     = NSLocalizedString("Dismiss",                                                                  comment: "dismissAlert")
        static let RecordingDisabledTitle           = NSLocalizedString("Recording Disabled",                                                       comment: "recordingDisabledTitle")
        static let RecordingDisabledMessage         = NSLocalizedString("You've disabled this app from recording your microphone. Check Settings.", comment: "recordingDisabledMessage")
        static let RecordingFailedTitle             = NSLocalizedString("Recording Failed",                                                         comment: "recordingFailedTitle")
        static let RecordingFailedMessage           = NSLocalizedString("Something went wrong with your recording.",                                comment: "recordingFailedMessage")
        static let AudioRecorderError               = NSLocalizedString("Audio Recorder Error",                                                     comment: "audioRecorderError")
        static let AudioSessionError                = NSLocalizedString("Audio Session Error",                                                      comment: "audioSessionError")
        static let AudioRecordingError              = NSLocalizedString("Audio Recording Error",                                                    comment: "audioRecordingError")
        static let AudioFileError                   = NSLocalizedString("Audio File Error",                                                         comment: "audioFileError")
        static let AudioEngineError                 = NSLocalizedString("Audio Engine Error",                                                       comment: "audioEngineError")
        static let ReminderSaveError                = NSLocalizedString("Error saving Reminder",                                                    comment: "reminderSaveError")
        static let ReminderDeleteErrorTitle         = NSLocalizedString("Error deleting Reminder",                                                  comment: "reminderDeleteErrorTitle")
        static let ReminderAttachmentDeleteError    = NSLocalizedString("Error deleting Reminder attachment.",                                      comment: "reminderAttachmentDeleteError")
        static let NotificationsDisabledTitle       = NSLocalizedString("Notifications Disabled",                                                   comment: "notificationsDisabledTitle")
        static let NotificationsDisabledMessage     = NSLocalizedString("You've disabled this app from displaying Notifications. Check Settings.",  comment: "notificationsDisabledMessage")
        static let NotificationsAuthorizationFailed = NSLocalizedString("Notifications Request Authorization Failed",                               comment: "notificationsAuthorizationFailed")
        static let NotificationsRequestFailed       = NSLocalizedString("Unable to Add Notification Request",                                       comment: "notificationsRequestFailed")
        static let NoCameraTitle                    = NSLocalizedString("No Camera",                                                                comment: "noCameraTitle")
        static let NoCameraMessage                  = NSLocalizedString("Sorry, this device has no camera.",                                        comment: "noCameraMessage")
        static let NoImageTitle                     = NSLocalizedString("No Image",                                                                 comment: "noImageTitle")
        static let NoImageMessage                   = NSLocalizedString("Sorry, Something went wrong.",                                             comment: "noImageMessage")
        static let CancelAlert                      = NSLocalizedString("The operation was canceled.",                                              comment: "cancelAlert")
        static let CancelAlertMessage               = NSLocalizedString("The save button was not pressed, cancelling",                              comment: "cancelAlertMessage")
        
    }
    
    
    func showAlert(title: String, message:String, vc: UIViewController)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction     = UIAlertAction(title: NSLocalizedString("OK",     comment: "ok"),      style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel"),  style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    
    //MARK -- Date Functions
    
    static func changeDateFormat(dateString: String, fromFormat: String, toFormat: String) ->String
    {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = fromFormat
        let date = inputDateFormatter.date(from: dateString)
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = toFormat
        return outputDateFormatter.string(from: date!)
    }
    
    
    func formattedDate(date: Date) ->String
    {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: date)
    }
    
    func formattedTime(date: Date, for24Hours: Bool) ->String
    {
        let formatter = DateFormatter()
        //formatter.timeStyle = .short
        
        if for24Hours
        {
            // For 24 Hrs
            formatter.dateFormat = "HH:mm"
        }
        else
        {
            // For 12 Hrs
            formatter.dateFormat = "h:mm a"
        }
        return formatter.string(from: date)
    }
    
    func formattedDateForSelectedDate(date: Date) ->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMMM yyyy"
        return formatter.string(from: date)
    }
    
    func formattedTimeForSelectedTime(date: Date) ->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }
    
   func formattedDateToSaveAsDate(dateString: String, timeString: String) ->Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, d MMMM yyyy HH:mm"
        
        guard let date = dateFormatter.date(from: (dateString + " " + timeString)) else
        {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        
        return date
    }
    
    
    func formatedDateAndTime(date: Date, for24Hours: Bool) ->String
    {
        let formatter = DateFormatter()
        
        if for24Hours
        {
            // For 24 Hrs
            formatter.dateFormat = "EEEE, d MMMM yyyy, HH:mm"
        }
        else
        {
            // For 12 Hrs
            formatter.dateFormat = "EEEE, d MMMM yyyy, h:mm a"
        }
        return formatter.string(from: date)
        
    }
    
    func deleteFile(fileURL : String, vc : UIViewController)
    {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: fileURL)
        {
            do
            {
                try fileManager.removeItem(atPath: fileURL)
            }
            catch let error as NSError
            {
                showAlert(title: Alerts.ReminderAttachmentDeleteError, message: String(describing: error.localizedDescription), vc: vc)
            }
        }
    }
    
    /*
    func updateByPrimaryKey (primaryKey : String, alarmStatus : Int)
    {
        let realm = try! Realm()
            do
            {
                try realm.write
                {
                    if let updateReminder = realm.object(ofType: Reminder.self, forPrimaryKey: primaryKey)
                    {
                        var newReminder = Reminder()
                        newReminder = updateReminder
                        //newReminder.reminderID = primaryKey
                        newReminder.alarm_status.value = alarmStatus
                        
                         realm.add(newReminder, update: true)
                    }
                    
                }
            }
            catch
            {
                print("Error updating reminder, \(error)")
            }
    }
    
    */
   



}
