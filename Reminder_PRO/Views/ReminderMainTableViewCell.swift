//
//  ReminderCell.swift
//  Reminder
//
//  Created by Boss on 15.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit


class ReminderMainTableViewCell: UITableViewCell
{
    
    //MARK: Properties

    @IBOutlet weak var reminderImageView: UIImageView!
    @IBOutlet weak var reminderTitleLabel: UILabel!
    @IBOutlet weak var reminderBodyLabel: UILabel!
    @IBOutlet weak var reminderDateTimeLabel: UILabel!
    
    var minHeight: CGFloat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        reminderImageView?.layer.cornerRadius = 30
        reminderImageView?.clipsToBounds = true
        reminderImageView?.contentMode = .scaleAspectFit
        reminderImageView?.backgroundColor = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
    {
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        guard let minHeight = minHeight else { return size }
        return CGSize(width: size.width, height: max(size.height, minHeight))
    }

    
}
