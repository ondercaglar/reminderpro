//
//  AppDelegate.swift
//  Reminder
//
//  Created by Boss on 13.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit
import UserNotifications
import Appirater
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Fix Nav Bar tint issue in iOS 15.0 or later - is transparent w/o code below
        // iOS 15 ve sonrasi Navigation Bar rengi transparan oluyordu. Onu engelliyor.
        if #available(iOS 15, *)
        {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.backgroundColor =  #colorLiteral(red: 0.3825020194, green: 0.599719286, blue: 0.9509775043, alpha: 1)
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
        
        
        
        // Override point for customization after application launch.
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: 1, migrationBlock:
        { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1)
                {
                    //write the migration logic here
                }
        })
        
        
        // Use Firebase library to configure APIs.
        //FirebaseApp.configure()
        
        // Initialize the Google Mobile Ads SDK.
        //GADMobileAds.configure(withApplicationID: Helper.app.admobAppID)
        //GADMobileAds.sharedInstance().start(completionHandler: nil)

        
        // set app delegate as notification center delegate
        UNUserNotificationCenter.current().delegate = self
        
        registerForRichNotifications()
        
        //Rating
        //Make sure you set [Appirater setDebug:NO] to ensure the request is not shown every time the app is launched
 
        Appirater.setAppId("1450941349")
        Appirater.setDaysUntilPrompt(7)
        Appirater.setUsesUntilPrompt(5)
        Appirater.setSignificantEventsUntilPrompt(-1)
        Appirater.setTimeBeforeReminding(2)
        Appirater.setDebug(false)
        Appirater.appLaunched(true)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
         UIApplication.shared.applicationIconBadgeNumber = 0
       
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    

    
    func registerForRichNotifications()
    {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error
            {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            if success
            {
                print("Permission granted")
            }
            else
            {
                print("Permission not granted")
            }
        }
        
        
        // Define Actions
        let actionShowDetails = UNNotificationAction(identifier: "showDetails", title: NSLocalizedString("Show Details", comment: "showDetails"),  options: [.foreground])
        let actionSnooze      = UNNotificationAction(identifier: "snooze",      title: NSLocalizedString("Snooze",       comment: "snooze"),       options: [])
        let actionStopRepeat  = UNNotificationAction(identifier: "stopRepeat",  title: NSLocalizedString("STOP Repeat",  comment: "stopRepeat"),   options: [])
        
        // Define Category
        let categoryNormal     = UNNotificationCategory(identifier: "normalReminder",     actions: [actionShowDetails, actionSnooze],     intentIdentifiers: [], options: [.customDismissAction])
        let categoryRepeatable = UNNotificationCategory(identifier: "repeatableReminder", actions: [actionShowDetails, actionStopRepeat], intentIdentifiers: [], options: [.customDismissAction])
        
        // Register Category
        UNUserNotificationCenter.current().setNotificationCategories([categoryNormal, categoryRepeatable])
    }
    

}



extension AppDelegate: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo
        let reminderID = userInfo["REMINDER_ID"] as! String
  
        switch response.actionIdentifier
        {
            case "showDetails":
            
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let rootViewController = self.window!.rootViewController as! UINavigationController
                
                if let reminderDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "ReminderDetailsVC") as? ReminderDetailsVC
                {
                    reminderDetailsVC.reminderID = reminderID
                    rootViewController.pushViewController(reminderDetailsVC, animated: false)
                }

            case "snooze":
                var for24Hours:Bool = false
                var snoozeDuration : Int = 5
                let title = response.notification.request.content.title
                let body  = response.notification.request.content.body
                let badge = response.notification.request.content.badge
                
                let defaults = UserDefaults.standard
                if (defaults.object(forKey: Helper.app.for24HoursKey) != nil)
                {
                    for24Hours = defaults.bool(forKey: Helper.app.for24HoursKey)
                }
                
                if (defaults.object(forKey: Helper.app.snoozeDurationKey) != nil)
                {
                    snoozeDuration = defaults.integer(forKey: Helper.app.snoozeDurationKey)
                }
                
                let scheduleDate = Date.init().adding(minutes: snoozeDuration)
                // Schedule Normal Alarm
                AlarmManagerHelper.app.scheduleNormalLocalNotification(reminderIdentifier: reminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: for24Hours, badge: badge ?? 0)
                        
            case "stopRepeat":
                
                AlarmManagerHelper.app.cancelLocalNotification(reminderIdentifier: reminderID)
            
            default:
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let rootViewController = self.window!.rootViewController as! UINavigationController
               
                if let reminderDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "ReminderDetailsVC") as? ReminderDetailsVC
                {
                    reminderDetailsVC.reminderID = reminderID
                    rootViewController.pushViewController(reminderDetailsVC, animated: false)
                }
            
        }
        
        
        completionHandler()
    }
    

}

