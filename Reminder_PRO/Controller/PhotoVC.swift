//
//  PhotoVC.swift
//  Reminder
//
//  Created by Boss on 18.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit

class PhotoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
     var photoPath: String = ""
     //var bannerView: GADBannerView!
    
    // UIImagePickerController is a view controller that lets a user pick media from their photo library.
    let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: photoPath)
        {
            photoImageView.image = UIImage(contentsOfFile: photoPath)?.fixedOrientation()
        }
        
        /*
        if(Helper.app.isInternetAvailable())
        {
            // Reklam Icin Eklendi
            // In this case, we instantiate the banner with desired ad size.
            
            bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            bannerView.adUnitID = Helper.app.bannerAdUnitID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            addBannerViewToView(bannerView)
        }*/
    }
    
    
    @IBAction func takePhotoBtnWasPressed(_ sender: UIButton)
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            imagePickerController.sourceType = .camera
            present(imagePickerController, animated: true, completion: nil)
        }
        else
        {
            Helper.app.showAlert(title: Helper.Alerts.NoCameraTitle, message: Helper.Alerts.NoCameraMessage, vc: self)
        }
    }
    
    @IBAction func galleryBtnWasPressed(_ sender: UIButton)
    {
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer)
    {
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else
        {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        photoImageView.contentMode = .scaleAspectFit
        photoImageView.clipsToBounds = true
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage.fixedOrientation()
        
        saveImage()
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    
    func saveImage()
    {
        let timestampFilename = String(Int(Date().timeIntervalSince1970)) + "_ReminderPhoto.png"
        
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        photoPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(timestampFilename)
        //get the image we took with camera
        let image = photoImageView.image!
        //get the PNG data for this image
        let data = image.pngData()
        //store it in the document directory
        fileManager.createFile(atPath: photoPath as String, contents: data, attributes: nil)
    }
    
    
    func getImage(imageName: String)
    {
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath)
        {
            photoImageView.image = UIImage(contentsOfFile: imagePath)?.fixedOrientation()
        }
        else
        {
             Helper.app.showAlert(title: Helper.Alerts.NoImageTitle, message: Helper.Alerts.NoImageMessage, vc: self)
        }
    }

    
    @IBAction func cancel(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else
        {
            Helper.app.showAlert(title: Helper.Alerts.CancelAlert, message: Helper.Alerts.CancelAlertMessage, vc: self)
            return
        }
    }
    
    
    
    //Reklam Icin Eklendi
    /*
    func addBannerViewToView(_ bannerView: GADBannerView)
    {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *)
        {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    } */

}
