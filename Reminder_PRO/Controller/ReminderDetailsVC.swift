//
//  ReminderDetailViewController.swift
//  Reminder
//
//  Created by Boss on 10.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation


class ReminderDetailsVC: UIViewController, AVAudioPlayerDelegate {

    let realm = try! Realm()

    @IBOutlet weak var detailsImageView: UIImageView!
    @IBOutlet weak var reminderName: UILabel!
    @IBOutlet weak var reminderNameLabel: UILabel!
    @IBOutlet weak var reminderDescription: UILabel!
    @IBOutlet weak var reminderDescriptionLabel: UILabel!
    @IBOutlet weak var reminderDateTime: UILabel!
    @IBOutlet weak var reminderDateTimeLabel: UILabel!
    @IBOutlet weak var reminderRepeatInterval: UILabel!
    @IBOutlet weak var reminderRepeatIntervalLabel: UILabel!
    
    @IBOutlet weak var recordPlayPauseButton: UIButton!
    @IBOutlet weak var recordPlaySlider: UISlider!
    @IBOutlet weak var recordPlayView: UIView!
    
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    let imgPlay  = UIImage(named: "play_circle_blue")
    let imgPause = UIImage(named: "pause_circle_blue")
    
    //var bannerView: GADBannerView!
    //var interstitial: GADInterstitial!
    var adCounter: Int = 1
    var mRemainder: Int = 0
    
    var audioPlayer:AVAudioPlayer?
    var isPlaying = false
    var timer: Timer?
    var recordedAudioURL:URL?
    var reminderID:String!
    var specificReminder: Reminder!
    var for24Hours:Bool!
    var pickerData: [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.applyRoundCorner(trashButton)
        self.applyRoundCorner(shareButton)
        self.applyRoundCorner(editButton)
        
        self.recordPlaySlider.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)

        loadReminder(myPrimaryKey: reminderID)
        
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: Helper.app.for24HoursKey) != nil)
        {
            for24Hours = defaults.bool(forKey: Helper.app.for24HoursKey)
        }
        else
        {
            for24Hours =  false
        }
    
        if (defaults.object(forKey: Helper.app.adCounterKey) != nil)
        {
            adCounter = defaults.integer(forKey: Helper.app.adCounterKey)
        }
        let addPlus = adCounter + 1
        defaults.set(addPlus, forKey: Helper.app.adCounterKey)
        
        if let path = Bundle.main.path(forResource: "RepeatArray", ofType: "plist")
        {
            if let dict = NSDictionary(contentsOfFile: path)
            {
                pickerData = dict.object(forKey: "repeatData") as! [String]
            }
        }
        
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: specificReminder.photo_path)
        {
            self.detailsImageView.image = UIImage(contentsOfFile: specificReminder.photo_path)?.fixedOrientation()
        }
        else
        {
            self.detailsImageView.image =  UIImage(named: "ic_reminder_6")
        }
        
        configureUI()
        
        /*
        if(Helper.app.isInternetAvailable())
        {
            // Reklam Icin Eklendi
            bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            bannerView.adUnitID = Helper.app.bannerAdUnitID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            addBannerViewToView(bannerView)
            
            interstitial = createAndLoadInterstitial()
        }*/
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setUpNavBar()
    }
    
    
    func configureUI()
    {
        reminderNameLabel.text = NSLocalizedString("Reminder Name :", comment: "NameField")
        reminderName.text = specificReminder.title
     
        reminderDescriptionLabel.text = NSLocalizedString("Description :", comment: "DescriptionField")
        reminderDescription.text = specificReminder.body
      
        reminderDateTimeLabel.text = NSLocalizedString("Reminder Date & Time :", comment: "DateField")
        reminderDateTime.text = Helper.app.formatedDateAndTime(date: specificReminder.reminder_date_time!, for24Hours: for24Hours)
       
        reminderRepeatIntervalLabel.text = NSLocalizedString("Repeat :", comment: "RepeatField")
        
        if let repeatInterval = specificReminder!.repeat_interval.value
        {
            reminderRepeatInterval.text = pickerData[repeatInterval]
        }
        else
        {
            reminderRepeatInterval.text = NSLocalizedString("Never", comment: "noRepeat")
        }
        
        
        if (specificReminder.recorded_sound_path != "")
        {
            recordPlayView.isHidden = false
            
            let recordedAudioURL = URL(string: specificReminder.recorded_sound_path)
            print(recordedAudioURL!)
            
            do
            {
                audioPlayer =  try AVAudioPlayer(contentsOf: recordedAudioURL!)
                recordPlaySlider.maximumValue = Float(audioPlayer?.duration ?? 0)
                
                self.audioPlayer?.delegate = self
                
                if timer == nil
                {
                    timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
                }
            }
            catch{
                Helper.app.showAlert(title: Helper.Alerts.AudioFileError, message: String(describing: error), vc: self)
            }
        }
    }
    
    
    @IBAction func trashButtonPressed(_ sender: Any)
    {
        showDeleteAlerts()
    }
    
    @IBAction func shareButtonPressed(_ sender: Any)
    {
        let dateTime = Helper.app.formatedDateAndTime(date: specificReminder.reminder_date_time!, for24Hours: self.for24Hours)
        let reminderName = NSLocalizedString("Reminder Name:", comment: "reminderName")
        let description  = NSLocalizedString("Description:",   comment: "description")
        let dateAndTime  = NSLocalizedString("Date & Time:",   comment: "dateAndTime")
        
        let defaultText: String = "\(reminderName) \(specificReminder.title) \n"
                                + "\(description) \(specificReminder.body) \n"
                                + "\(dateAndTime) \(dateTime)"
        
        let activityController = UIActivityViewController(activityItems:[defaultText], applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func editButtonPressed(_ sender: Any)
    {
        if let reminder =  specificReminder
        {
            performSegue(withIdentifier: "editReminder", sender: reminder)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let destinationNavigationController = segue.destination as! UINavigationController
        let addReminderVC =  destinationNavigationController.topViewController as! AddReminderVC
        destinationNavigationController.modalPresentationStyle = .fullScreen
        //addReminderVC.modalPresentationStyle = .fullScreen
        assert(sender as? Reminder != nil)
        addReminderVC.reminder = sender as? Reminder
        
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func playPauseRecord(_ sender: UIButton)
    {
        if isPlaying
        {
            sender.setImage(imgPlay, for: .normal)
            audioPlayer?.pause()
            isPlaying = false
        }
        else
        {
            sender.setImage(imgPause, for: .normal)
            audioPlayer?.play()
            isPlaying = true
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        audioPlayer?.stop()
        recordPlaySlider.value = 0
        recordPlayPauseButton.setImage(imgPlay, for: .normal)
        isPlaying = false
    }
    
    
    @objc func updateSlider()
    {
        recordPlaySlider.value = Float(audioPlayer?.currentTime ?? 0)
    }
    
    @IBAction func changeRecordedAudioTime(_ sender: Any)
    {
        audioPlayer?.stop()
        audioPlayer?.currentTime = TimeInterval(recordPlaySlider.value)
        audioPlayer?.prepareToPlay()
        audioPlayer?.play()
    }
    
    func loadReminder(myPrimaryKey:String)
    {
        specificReminder = realm.object(ofType: Reminder.self, forPrimaryKey: myPrimaryKey)
    }
    
    
    func setUpNavBar()
    {
        //For title in navigation bar
        self.navigationController?.view.tintColor = UIColor.white
        self.navigationItem.title = NSLocalizedString("Details", comment: "ReminderDetailsVC Title")
        
        //For back button in navigation bar
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    func applyRoundCorner(_ object:AnyObject)
    {
        let object:UIView = object as! UIView
        let frame:CGRect? = object.frame
        
        object.layer.cornerRadius = frame!.size.width / 2
        object.layer.masksToBounds = true
    }
    
    
    func showDeleteAlerts()
    {
        let alert = UIAlertController(title: NSLocalizedString("Would you like to delete this reminder?", comment: "deleteMessage"), message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel"),  style: .cancel,      handler: nil)
        let deleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: "delete"),  style: .destructive, handler:
        { action in

            AlarmManagerHelper.app.cancelLocalNotification(reminderIdentifier: self.specificReminder.reminderID)
            
            let photoPath  = self.specificReminder.photo_path
            let recordPath = self.specificReminder.recorded_sound_path
            
            do
            {
                try self.realm.write
                {
                    self.realm.delete(self.specificReminder)
                }
            }
            catch
            {
                Helper.app.showAlert(title: Helper.Alerts.ReminderDeleteErrorTitle, message: String(describing: error.localizedDescription), vc: self)
            }
            
            Helper.app.deleteFile(fileURL: photoPath,  vc: self)
            Helper.app.deleteFile(fileURL: recordPath, vc: self)
            
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }
    
    /*
    func createAndLoadInterstitial() -> GADInterstitial?
    {
        interstitial = GADInterstitial(adUnitID: Helper.app.interstitialAdUnitId)
        
        guard let interstitial = interstitial else {
            return nil
        }
        
        let request = GADRequest()
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial)
    {
        mRemainder = adCounter % 20
        if (mRemainder == 0)
        {
            ad.present(fromRootViewController: self)
        }
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial)
    {
        adCounter += 1
    }
    
    
    //Reklam Icin Eklendi
    func addBannerViewToView(_ bannerView: GADBannerView)
    {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *)
        {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }*/
    
    
    

  


    
    

    
    

}
