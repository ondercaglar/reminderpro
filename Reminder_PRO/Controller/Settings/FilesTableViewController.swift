//
//  FilesTableViewController.swift
//  Reminder
//
//  Created by Boss on 6.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import UIKit
import AVFoundation

class FilesTableViewController: UITableViewController {
    // MARK: - Class Variables
    
    ///App Controller singleton
    let appController = AppController.sharedInstance()
    
    ///The directory where sound files are located.
    var directory: NSDictionary!
    
    ///The files in the directory
    var files: [String] = []
    
    
    // MARK: - View Controller Setup
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if #available(iOS 11.0, *)
        {
            self.navigationItem.largeTitleDisplayMode = .always
        }
        files = directory["files"] as! [String]
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setUpNavBar()
        
        appController.userDefaults.synchronize()
        if let notificationSoundDictionary = appController.userDefaults.dictionary(forKey: Helper.app.dictionaryNotificationSoundKey)
        {
            appController.dictionaryNotificationSound["fileName"] = notificationSoundDictionary["fileName"] as? String
            appController.dictionaryNotificationSound["filePath"] = notificationSoundDictionary["filePath"] as? String
        }
    }
    

    override func viewWillDisappear(_ animated: Bool)
    {
        appController.userDefaults.set(appController.dictionaryNotificationSound, forKey: Helper.app.dictionaryNotificationSoundKey)
        appController.userDefaults.synchronize()
    
        let fileURL: URL = URL(fileURLWithPath: appController.dictionaryNotificationSound["filePath"]!)
        let fileName = appController.dictionaryNotificationSound["fileName"]
        
        do
        {
            let targetURL = try FileManager.default.soundsLibraryURL(for: fileName!)
            // copy audio file to /Library/Sounds
            if !FileManager.default.fileExists(atPath: targetURL.path)
            {
                do{
                    try FileManager.default.copyItem(at: fileURL, to: targetURL)
                }
                catch {
                    print(error)
                }
            }
        }
        catch {
            print(error)
        }
        
        //Stop sounds if playing when exiting.
        appController.audioPlayer?.stop()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return directory["path"] as? String
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return files.count
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        let dir: String = directory["path"] as! String
        let fileName: String = files[indexPath.row]
        let filePath: String = "\(dir)/\(fileName)"
        let cell = tableView.dequeueReusableCell(withIdentifier: "sound", for: indexPath) as! SoundTableViewCell
        
        let fileNameSaved = appController.dictionaryNotificationSound["fileName"] ?? ""
        //print(fileNameSaved)
        //print(fileName)
        
        if (fileNameSaved == fileName)
        {
            cell.accessoryType = .checkmark
            //print("yes")
        }
        else
        {
            cell.accessoryType = .none
            //print("no")
        }
    
        cell.fileNameLabel.text = fileName
        cell.filePathLabel.text = filePath
        
        return cell
     }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //Play the sound
        let dir: String = directory["path"] as! String
        let fileName: String = files[indexPath.row]
        let filePath: String = "\(dir)/\(fileName)"
        let fileURL: URL = URL(fileURLWithPath: filePath)
        do
        {   appController.audioPlayer = try AVAudioPlayer(contentsOf: fileURL)
            appController.audioPlayer!.play()
        }
        catch
        {
            debugPrint("\(error)")
        }
        
        appController.dictionaryNotificationSound["fileName"] = fileName
        appController.dictionaryNotificationSound["filePath"] = filePath
        
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func setUpNavBar()
    {
        //For title in navigation bar
        //self.navigationController?.view.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        self.navigationController?.view.tintColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)

        //For back button in navigation bar
        let backButton = UIBarButtonItem()
        backButton.title = NSLocalizedString("Back", comment: "back")
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
}


extension FileManager
{
    func soundsLibraryURL(for filename: String) throws -> URL
    {
        let libraryURL = try url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let soundFolderURL = libraryURL.appendingPathComponent("Sounds", isDirectory: true)
        if !fileExists(atPath: soundFolderURL.path)
        {
            try createDirectory(at: soundFolderURL, withIntermediateDirectories: true)
        }
        return soundFolderURL.appendingPathComponent(filename, isDirectory: false)
    }
}
