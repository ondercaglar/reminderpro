//
//  AppController.swift
//  Reminder
//
//  Created by Boss on 6.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//


import UIKit
import AVFoundation

class AppController: NSObject
{
    ///Audio player responsible for playing sound files.
    var audioPlayer: AVAudioPlayer?
    
    ///User's selected sound files.
    var dictionaryNotificationSound = ["fileName": "", "filePath": ""]
    
    ///User defaults
    var userDefaults: UserDefaults = UserDefaults.standard
    class func sharedInstance() -> AppController
    {
        return appControllerSingletonGlobal
    }
}

///Model singleton so that we can refer to this from throughout the app.
let appControllerSingletonGlobal = AppController()
