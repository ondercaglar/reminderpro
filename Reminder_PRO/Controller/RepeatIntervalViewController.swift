//
//  RepeatIntervelViewController.swift
//  Reminder
//
//  Created by Boss on 27.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit

class RepeatIntervalViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerRepeatInterval: UIPickerView!
    
    var pickerData: [String] = [String]()
    var onSetInterval: ((_ data: String, _ repeatIntervalRow :Int) -> ())?
    var selectedInterval: String = NSLocalizedString("Never",     comment: "noRepeat")
    var selectedRepeatIntervalRow :Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Connect data:
        self.pickerRepeatInterval.delegate = self
        self.pickerRepeatInterval.dataSource = self
        
        
        if let path = Bundle.main.path(forResource: "RepeatArray", ofType: "plist")
        {
            if let dict = NSDictionary(contentsOfFile: path)
            {
                pickerData = dict.object(forKey: "repeatData") as! [String]
            }
        }

    }
    
    
    @IBAction func setInterval_TouchUpInside(_ sender: UIButton)
    {
        onSetInterval?(selectedInterval, selectedRepeatIntervalRow)
        dismiss(animated: true)
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerData.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickerData[row]
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
      selectedInterval = pickerData[row]
      selectedRepeatIntervalRow = row
    }
    
}

