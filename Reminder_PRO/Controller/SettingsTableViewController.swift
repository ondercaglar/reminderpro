//
//  SettingsTableViewController.swift
//  Reminder
//
//  Created by Boss on 6.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var timeDisplaySwitch: UISwitch!
    @IBOutlet weak var sortByDateSwitch: UISwitch!
    
    //var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        if (defaults.object(forKey: Helper.app.for24HoursKey) != nil)
        {
            timeDisplaySwitch.isOn = defaults.bool(forKey: Helper.app.for24HoursKey)
        }
        else
        {
            timeDisplaySwitch.isOn = false
        }
        
        if (defaults.object(forKey: Helper.app.sortByDateKey) != nil)
        {
            sortByDateSwitch.isOn = defaults.bool(forKey: Helper.app.sortByDateKey)
        }
        else
        {
            sortByDateSwitch.isOn = false
        }
        
        //tableView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setUpNavBar()
        
        /*
        if(Helper.app.isInternetAvailable())
        {
            // Reklam Icin Eklendi
            // In this case, we instantiate the banner with desired ad size.
            
            bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            bannerView.adUnitID = Helper.app.bannerAdUnitID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            addBannerViewToView(bannerView)
        }*/
    }
    
    
    @IBAction func TimeDisplayPreferenceSwitchStateChanged(_ sender: UISwitch)
    {
        let defaults = UserDefaults.standard
        
        if (sender.isOn == true)
        {
            defaults.set(true, forKey: Helper.app.for24HoursKey)
        }
        else
        {
            defaults.set(false, forKey: Helper.app.for24HoursKey)
        }
    }
    
    
    @IBAction func SortByDateSwitchStateChanged(_ sender: UISwitch)
    {
        let defaults = UserDefaults.standard
        
        if (sender.isOn == true)
        {
            defaults.set(true, forKey: Helper.app.sortByDateKey)
        }
        else
        {
            defaults.set(false, forKey: Helper.app.sortByDateKey)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0 && indexPath.row == 2
        {
           showSnoozeAlerts()
        }
    }
    
    
    func setUpNavBar()
    {
        //For title in navigation bar
        self.navigationController?.view.tintColor = UIColor.white
        
        //For back button in navigation bar
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    
    
    func showSnoozeAlerts()
    {
        let defaults = UserDefaults.standard
        
        var snoozeDuration : Int?
        
        if (defaults.object(forKey: Helper.app.snoozeDurationKey) != nil)
        {
            snoozeDuration = defaults.integer(forKey: Helper.app.snoozeDurationKey)
        }
        else
        {
            snoozeDuration = 5
        }
        
        let alert = UIAlertController(title: NSLocalizedString("Set Snooze Duration", comment: "setSnoozeDuration") , message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel"), style: .cancel, handler: nil))
        
        alert.addTextField(configurationHandler:
        {
            textField in
            textField.text = "\(snoozeDuration ?? 5)"
            textField.keyboardType = UIKeyboardType.numberPad
        })
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "ok"), style: .default, handler:
        {
            action in
            
            if let snoozeDuration = alert.textFields?.first?.text
            {
                defaults.set(Int(snoozeDuration), forKey: Helper.app.snoozeDurationKey)
            }
        }))
        
        self.present(alert, animated: true)
    }
    
    
    
    //Reklam Icin Eklendi
    /*
    func addBannerViewToView(_ bannerView: GADBannerView)
    {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *)
        {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }*/

    
}
