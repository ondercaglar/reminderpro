//
//  ReminderMainVC.swift
//  Reminder
//
//  Created by Boss on 14.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications
import ChameleonFramework

class ReminderMainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UIAlertViewDelegate {
    
    let realm = try! Realm()
    
    var reminders: Results<Reminder>?
    var searchResults: Results<Reminder>?
    var searchController:UISearchController!
    var for24Hours:Bool!
    var sortByDate:Bool!
    let minimumHeight = CGFloat(80)
    
    /*
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView.adUnitID = Helper.app.bannerAdUnitID
        adBannerView.delegate = self
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
     */
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //print(Realm.Configuration.defaultConfiguration.fileURL)
       
        //adBannerView.load(GADRequest())
        
        tableView.delegate   = self
        tableView.dataSource = self
        
        searchController = UISearchController(searchResultsController: nil)
        tableView.tableHeaderView = searchController.searchBar
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("Search reminders...", comment: "SearchField")
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .singleLine
        
        // Enable self sizing cells
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setUpNavBar()
        
        self.segmentedControl.tintColor = #colorLiteral(red: 0.2745098039, green: 0.5333333333, blue: 0.9490196078, alpha: 1)
        segmentedControl.selectedSegmentIndex = 0
        
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: Helper.app.for24HoursKey) != nil)
        {
            for24Hours = defaults.bool(forKey: Helper.app.for24HoursKey)
        }
        else
        {
            for24Hours =  false
        }
        
        if (defaults.object(forKey: Helper.app.sortByDateKey) != nil)
        {
            sortByDate = defaults.bool(forKey: Helper.app.sortByDateKey)
        }
        else
        {
            sortByDate = false
        }
        
        loadReminders()
        
        searchController.hidesNavigationBarDuringPresentation = false
    
    }
       
    /*
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        //Reposition the banner ad to create a slide down effect
        let translateTransform = CGAffineTransform(translationX: 0, y: bannerView.bounds.size.height)
        bannerView.transform = translateTransform
        UIView.animate(withDuration: 0.5)
        {
            //self.tableView.tableFooterView?.frame = bannerView.frame
            bannerView.transform = CGAffineTransform.identity
            //self.tableView.tableFooterView = bannerView
        }
    }
     */
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if searchController.isActive
        {
            searchController.isActive = false
            searchController.dismiss(animated: false, completion: nil)
        }
    }
    
    
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl)
    {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            loadReminders()
        case 1:
            loadTodayReminders()
        case 2:
            loadTomorrowReminders()
        default:
            break
        }
    }
    
    
    func filterContentForSearchText(searchText: String)
    {
        searchResults = reminders?.filter("title CONTAINS[cd] %@  OR body CONTAINS[cd] %@",searchText, searchText).sorted(byKeyPath: "reminder_date_time", ascending: true)
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        if let searchText = searchController.searchBar.text
        {
            filterContentForSearchText(searchText: searchText)
            tableView.reloadData()
        }
    }

    
    // MARK - TableView Datasource Methods
    
    /*
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return adBannerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return adBannerView.frame.height
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchController.isActive
        {
            return searchResults?.count ?? 1
        }
        else
        {
            return reminders?.count ?? 1
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell") as? ReminderMainTableViewCell else
        {
            return UITableViewCell()
        }
        
        if let reminder = (searchController.isActive) ? searchResults?[indexPath.row] : reminders?[indexPath.row]
        {
            cell.reminderTitleLabel.text    = reminder.title
            cell.reminderBodyLabel.text     = reminder.body
            cell.reminderDateTimeLabel.text = Helper.app.formatedDateAndTime(date: reminder.reminder_date_time!, for24Hours: for24Hours)
   
            let fileManager = FileManager.default
            
            if (reminder.alarm_status.value == 2)
            {
                let color = UIColor.white
                cell.backgroundColor =  #colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 0.7529965753)
                cell.reminderTitleLabel.textColor    = color
                cell.reminderBodyLabel.textColor     = color
                cell.reminderDateTimeLabel.textColor = color
                
                if fileManager.fileExists(atPath: reminder.photo_path)
                {
                    cell.reminderImageView.image = UIImage(contentsOfFile: reminder.photo_path)
                }
                else
                {
                    cell.reminderImageView.image =  UIImage(named: "ico_repeat")
                }
            }
            else if (reminder.alarm_status.value == 1)
            {
                let now = Date()
                if (reminder.reminder_date_time! > now)
                {
                    let color = UIColor.black
                    cell.backgroundColor = UIColor.white
                    cell.reminderTitleLabel.textColor    = color
                    cell.reminderBodyLabel.textColor     = color
                    cell.reminderDateTimeLabel.textColor = color
                    
                    
                    if fileManager.fileExists(atPath: reminder.photo_path)
                    {
                        cell.reminderImageView.image = UIImage(contentsOfFile: reminder.photo_path)?.fixedOrientation()
                    }
                    else
                    {
                        cell.reminderImageView.image =  UIImage(named: "ico_normal")
                    }
                }
                else
                {
                    let color = UIColor.black
                    cell.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
                    cell.reminderTitleLabel.textColor    = color
                    cell.reminderBodyLabel.textColor     = color
                    cell.reminderDateTimeLabel.textColor = color
                    
                    if fileManager.fileExists(atPath: reminder.photo_path)
                    {
                        cell.reminderImageView.image = UIImage(contentsOfFile: reminder.photo_path)?.fixedOrientation()
                    }
                    else
                    {
                        cell.reminderImageView.image =  UIImage(named: "ico_stop")
                    }
                }
            }
            
            if let color = UIColor(hexString: reminder.reminderColor)
            {
                cell.backgroundColor = color
                cell.reminderTitleLabel.textColor    = ContrastColorOf(color, returnFlat: true)
                cell.reminderBodyLabel.textColor     = ContrastColorOf(color, returnFlat: true)
                cell.reminderDateTimeLabel.textColor = ContrastColorOf(color, returnFlat: true)
            }
               
            cell.clipsToBounds = true
            cell.minHeight = 80
       }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let reminder = (searchController.isActive) ? searchResults?[indexPath.row] : reminders?[indexPath.row]
        {
            performSegue(withIdentifier: "showDetails", sender: reminder.reminderID)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let reminderDetailsVC = segue.destination as? ReminderDetailsVC
        {
            reminderDetailsVC.modalPresentationStyle = .fullScreen
            
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            barBtn.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            navigationItem.backBarButtonItem = barBtn
            
            assert(sender as? String != nil)
            reminderDetailsVC.reminderID = sender as? String
        }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if searchController.isActive
        {
            return []
        }
        else
        {
            let deleteAction = UITableViewRowAction(style: .destructive, title: NSLocalizedString("DELETE", comment: "Delete"))
            {
                (rowAction, indexPath) in
                self.updateModel(at: indexPath)
            }
            
            // Social Sharing Button
            let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: NSLocalizedString("SHARE", comment: "Share"))
            {
                (rowAction, indexPath) in
                let dateTime = Helper.app.formatedDateAndTime(date: self.reminders![indexPath.row].reminder_date_time!, for24Hours: self.for24Hours)
                let reminderName = NSLocalizedString("Reminder Name:", comment: "reminderName")
                let description  = NSLocalizedString("Description:",   comment: "description")
                let dateAndTime  = NSLocalizedString("Date & Time:",   comment: "dateAndTime")
                
                let defaultText: String = "\(reminderName) \(self.reminders![indexPath.row].title) \n"
                                        + "\(description) \(self.reminders![indexPath.row].body) \n"
                                        + "\(dateAndTime) \(dateTime)"
             
                let activityController = UIActivityViewController(activityItems:[defaultText], applicationActivities: nil)
                self.present(activityController, animated: true, completion: nil)
            }
            
            
            deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            shareAction.backgroundColor  = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            
            return [deleteAction, shareAction]
        }
    }
    


    // MARK - Data Manipulation Methods
    
    func loadReminders()
    {
        reminders = realm.objects(Reminder.self).sorted(byKeyPath: "reminder_date_time", ascending: sortByDate)
        tableView.reloadData()
        
        if reminders!.count >= 1
        {
            tableView.isHidden = false
        }
        else
        {
            tableView.isHidden = true
        }
    }
    
    
    func loadTodayReminders()
    {
        let todayStart = Calendar.current.startOfDay(for: Date())
        let todayEnd: Date = {
            let components = DateComponents(day: 1, second: -1)
            return Calendar.current.date(byAdding: components, to: todayStart)!
        }()
        
        reminders = realm.objects(Reminder.self).sorted(byKeyPath: "reminder_date_time", ascending: sortByDate).filter("reminder_date_time BETWEEN %@", [todayStart, todayEnd])
        tableView.reloadData()
        
        if reminders!.count >= 1
        {
            tableView.isHidden = false
        }
        else
        {
            tableView.isHidden = true
        }
    }
    

    func loadTomorrowReminders()
    {
        let todayStart = Calendar.current.startOfDay(for: Date.tomorrow)
        let todayEnd: Date = {
            let components = DateComponents(day: 1, second: -1)
            return Calendar.current.date(byAdding: components, to: todayStart)!
        }()
        
        reminders = realm.objects(Reminder.self).sorted(byKeyPath: "reminder_date_time", ascending: sortByDate).filter("reminder_date_time BETWEEN %@", [todayStart, todayEnd])
        tableView.reloadData()
        
        if reminders!.count >= 1
        {
            tableView.isHidden = false
        }
        else
        {
            tableView.isHidden = true
        }
    }
    
    
    //MARK: - Delete Data From Swipe
    
    func updateModel(at indexPath: IndexPath)
    {
        let alert = UIAlertController(title: NSLocalizedString("Would you like to delete this reminder?", comment: "deleteMessage"), message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel"),  style: .cancel,      handler: nil)
        let deleteAction = UIAlertAction(title: NSLocalizedString("Delete", comment: "delete"),  style: .destructive, handler:
        { action in
            
            if let reminder = self.reminders?[indexPath.row]
            {
                AlarmManagerHelper.app.cancelLocalNotification(reminderIdentifier: reminder.reminderID)
                
                let photoPath  = reminder.photo_path
                let recordPath = reminder.recorded_sound_path
                
                do
                {
                    try self.realm.write
                    {
                        self.realm.delete(reminder)
                    }
                }
                catch
                {
                    Helper.app.showAlert(title: Helper.Alerts.ReminderDeleteErrorTitle, message: String(describing: error.localizedDescription), vc: self)
                }
                
                Helper.app.deleteFile(fileURL: photoPath,  vc: self)
                Helper.app.deleteFile(fileURL: recordPath, vc: self)
                
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                self.loadReminders()
            }
        })
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }

    
    
    func setUpNavBar()
    {
        //For title in navigation bar
        self.navigationController?.view.tintColor = UIColor.white
        
        //For back button in navigation bar
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        self.navigationController?.hidesBarsOnSwipe = false
    }



}



    

    
    

    
 
