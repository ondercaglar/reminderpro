//
//  ColorPickerViewController.swift
//  Reminder
//
//  Created by Boss on 27.01.2019.
//  Copyright © 2019 Boss. All rights reserved.
//

import UIKit
import ChameleonFramework

class ColorPickerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet var collectionView: UICollectionView!
    @IBAction func noColorBtnClicked(_ sender: Any) {
        
        let selectedColor = UIColor.white
        onSetColor?(selectedColor)
        dismiss(animated: true)
    }
    
    var onSetColor: ((_ color: UIColor) -> ())?
    private var colorArray = [FlatRed(),         FlatOrange(),          FlatYellow(),      FlatSand(),       FlatNavyBlue(),       FlatBlack(),
                              FlatMagenta(),     FlatTeal(),            FlatSkyBlue(),     FlatGreen(),      FlatMint(),           FlatWhite(),
                              FlatGray(),        FlatForestGreen(),     FlatPurple(),      FlatBrown(),      FlatPlum(),           FlatWatermelon(),
                              FlatLime(),        FlatPink(),            FlatMaroon(),      FlatCoffee(),     FlatPowderBlue(),     FlatBlue(),
                              FlatRedDark(),     FlatOrangeDark(),      FlatYellowDark(),  FlatSandDark(),   FlatNavyBlueDark(),   FlatBlackDark(),
                              FlatMagentaDark(), FlatTealDark(),        FlatSkyBlueDark(), FlatGreenDark(),  FlatMintDark(),       FlatWhiteDark(),
                              FlatGrayDark(),    FlatForestGreenDark(), FlatPurpleDark(),  FlatBrownDark(),  FlatPlumDark(),       FlatWatermelonDark(),
                              FlatLimeDark(),    FlatPinkDark(),        FlatMaroonDark(),  FlatCoffeeDark(), FlatPowderBlueDark(), FlatBlueDark()]
    
    // MARK: - UICollectionViewDataSource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.backgroundColor = colorArray[indexPath.row]
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout protocol
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let sideSize = (traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular) ? 80.0 : 128.0
        
        let collectionViewSize = collectionView.frame.size
        let collectionViewArea = Double(collectionViewSize.width * collectionViewSize.height)
        
        let sideSize: Double = sqrt(collectionViewArea / (Double(colorArray.count))) - 10.0
        
        return CGSize(width: sideSize, height: sideSize)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let selectedColor = colorArray[indexPath.row]
        
        onSetColor?(selectedColor)
        dismiss(animated: true)
    }
    
    
    // MARK: - TraitCollection related methods
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        collectionView.reloadData()
    }


}
