//
//  AddReminderVC.swift
//  Reminder
//
//  Created by Boss on 14.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications
import AVFoundation
import ChameleonFramework

class AddReminderVC: UIViewController, UITextViewDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    let realm = try! Realm()
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var btnSelectRepeatInterval: UIButton!
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordDeleteButton: UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var recordPlaySlider: UISlider!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var recordPlayView: UIView!
    @IBOutlet weak var saveUpdateBtn: UIButton!
    @IBOutlet weak var colorPickerBtn: DesignableButton!
    
    let imgPlay  = UIImage(named: "play_circle_blue")
    let imgPause = UIImage(named: "pause_circle_blue")
    
    //var bannerView: GADBannerView!
    var photoPath: String = ""
    var reminder_date: Date?
    var selectedRepeatIntervalRow :Int = 0
    var selectedDate : String = ""
    var selectedTime : String = ""
    var selectedReminderColor :String = ""
    var for24Hours:Bool!
    var isChecked = true
    var audioRecorder: AVAudioRecorder?
    var audioPlayer:AVAudioPlayer?
    var isPlaying = false
    var timer: Timer?
    var recordedAudioURLtoSave:URL?
    var reminder: Reminder?
    var pickerData: [String] = [String]()
    var timestampReminderID = String(Int(Date().timeIntervalSince1970))


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.recordPlaySlider.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: Helper.app.for24HoursKey) != nil)
        {
            for24Hours = defaults.bool(forKey: Helper.app.for24HoursKey)
        }
        else
        {
            for24Hours =  false
        }
        
        bodyTextView.text = NSLocalizedString("Description :", comment: "DescriptionField")
        
        if #available(iOS 13.0, *)
        {
            bodyTextView.textColor = .placeholderText
        }
        else
        {
            // Fallback on earlier versions
            bodyTextView.textColor = UIColor.lightGray
        }
        
        bodyTextView.font = UIFont.systemFont(ofSize: 18.0)
        bodyTextView.returnKeyType = .done
        bodyTextView.delegate = self
        
        reminder_date = Date()
        
        selectedDate = Helper.app.formattedDateForSelectedDate(date: reminder_date ?? Date())
        selectedTime = Helper.app.formattedTimeForSelectedTime(date: reminder_date ?? Date())
        
        recordDeleteButton.isEnabled = false
        
        addImageButton.setImage(UIImage(named:"camera.png"), for: .normal)
        addImageButton.imageView?.contentMode = .scaleAspectFit
        addImageButton.imageEdgeInsets = UIEdgeInsets(top: 5,left: 0,bottom: 5,right: 0)
        
        recordButton.setImage(UIImage(named:"microphone.png"), for: .normal)
        recordButton.imageView?.contentMode = .scaleAspectFit
        recordButton.imageEdgeInsets = UIEdgeInsets(top: 5,left: 0,bottom: 5,right: 0)
        
        recordDeleteButton.setImage(UIImage(named:"delete.png"), for: .normal)
        recordDeleteButton.imageView?.contentMode = .scaleAspectFit
        recordDeleteButton.imageEdgeInsets = UIEdgeInsets(top: 5,left: 0,bottom: 5,right: 0)
        
        
        if(reminder != nil)
        {
            if let path = Bundle.main.path(forResource: "RepeatArray", ofType: "plist")
            {
                if let dict = NSDictionary(contentsOfFile: path)
                {
                    pickerData = dict.object(forKey: "repeatData") as! [String]
                }
            }
            
            updateConfiguration ()
        }
    }
    
    /*
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if(Helper.app.isInternetAvailable())
        {
            // Reklam Icin Eklendi
            bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            bannerView.adUnitID = Helper.app.bannerAdUnitID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            addBannerViewToView(bannerView)
        }
    }
    */
    
    
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == NSLocalizedString("Description :", comment: "DescriptionField")
        {
            textView.text = ""
            
            if #available(iOS 13.0, *)
            {
                textView.textColor = .label
            }
            else
            {
                // Fallback on earlier versions
               textView.textColor = UIColor.black
            }
        
            textView.font = UIFont.systemFont(ofSize: 18.0)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text == ""
        {
            textView.text = NSLocalizedString("Description :", comment: "DescriptionField")
            
            if #available(iOS 13.0, *)
             {
                 textView.textColor = .placeholderText
             }
             else
             {
                 // Fallback on earlier versions
                textView.textColor = UIColor.lightGray
             }
            textView.font = UIFont.systemFont(ofSize: 14.0)
        }
    }

    
    @IBAction func dateBtnWasPressed(_ sender: UIButton)
    {
        let sb = UIStoryboard(name: "DateTimePopupViewController", bundle: nil)
        let popup = sb.instantiateInitialViewController()! as! DateTimePopupViewController
        popup.showTimePicker = false
        self.present(popup, animated: true)
        popup.onSave = {(data1, data2) in
            self.dateButton.setTitle(data1, for: .normal)
            self.selectedDate = data2
        }
    }
    
    @IBAction func timeBtnWasPressed(_ sender: UIButton)
    {
        let sb = UIStoryboard(name: "DateTimePopupViewController", bundle: nil)
        let popup = sb.instantiateInitialViewController()! as! DateTimePopupViewController
        popup.showTimePicker = true
        popup.for24Hours = for24Hours
        popup.onSave = {(data1, data2) in
            self.timeButton.setTitle(data1, for: .normal)
            self.selectedTime = data2
        }
        self.present(popup, animated: true)
    }
    
    @IBAction func selectRepeatIntervalBtnWasPressed(_ sender: UIButton)
    {
        let sb = UIStoryboard(name: "RepeatIntervalViewController", bundle: nil)
        let popup = sb.instantiateInitialViewController()! as! RepeatIntervalViewController
        popup.onSetInterval = {(data, repeatIntervalRow) in
            self.btnSelectRepeatInterval.setTitle(data, for: .normal)
            self.selectedRepeatIntervalRow = repeatIntervalRow
        }
        self.present(popup, animated: true)
    }
    
    @IBAction func addPhotoBtnPressed(_ sender: UIButton)
    {
        performSegue(withIdentifier: "addViewPhoto", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        let destinationNavigationController = segue.destination as! UINavigationController
        if let photoVC =  destinationNavigationController.topViewController as? PhotoVC
        {
            photoVC.photoPath = photoPath
        }
    }
    
    
    @IBAction func colorPickerBtnPressed(_ sender: Any)
    {
        let sb = UIStoryboard(name: "ColorPickerStoryboard", bundle: nil)
        let popup = sb.instantiateInitialViewController()! as! ColorPickerViewController
        
        popup.onSetColor = {(color) in
            
            if (color == UIColor.white)
            {
                self.colorPickerBtn.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
                self.colorPickerBtn.setTitleColor(UIColor.white, for: .normal)
                self.selectedReminderColor = ""
            }
            else
            {
                self.colorPickerBtn.backgroundColor = color
                self.colorPickerBtn.setTitleColor(ContrastColorOf(color, returnFlat: true), for: .normal)
                self.selectedReminderColor = color.hexValue()
            }
        }
        self.present(popup, animated: true)
    }
    
    
    
    @IBAction func recordButtonPressed(_ sender: UIButton)
    {
        if isChecked
        {
            sender.setTitle(NSLocalizedString("STOP", comment: "StopField"), for: .normal)
            
            configureRecordindState(true)
            
            let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
            let recordingName = "RecordedVoice_\(timestampReminderID).wav"
            let pathArray = [dirPath, recordingName]
            let filePath = URL(string: pathArray.joined(separator: "/"))
            
            print(recordingName)
            
            let session = AVAudioSession.sharedInstance()
            try! session.setCategory(.playAndRecord, mode: .default)
            
            try! audioRecorder = AVAudioRecorder(url: filePath!, settings: [:])
            audioRecorder?.delegate = self
            audioRecorder?.isMeteringEnabled = true
            audioRecorder?.prepareToRecord()
            audioRecorder?.record()
        }
        else
        {
            sender.setTitle(NSLocalizedString("RECORD", comment: "RecordField"), for: .normal)
              
            configureRecordindState(false)
            
            audioRecorder?.stop()
            let audioSession = AVAudioSession.sharedInstance()
            try! audioSession.setActive(false)
        }
        
         isChecked = !isChecked
    }
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool)
    {
        if flag
        {
            recordPlayView.isHidden = false
            
            let recordedAudioURL = audioRecorder?.url
            recordedAudioURLtoSave = recordedAudioURL
            
            prepareAudioPlayer(recordedAudioURL: recordedAudioURL!)
        }
        else
        {
            Helper.app.showAlert(title: Helper.Alerts.RecordingFailedTitle, message: Helper.Alerts.RecordingFailedMessage, vc: self)
        }
    }
    
    
    func prepareAudioPlayer(recordedAudioURL: URL)
    {
        do
        {
            audioPlayer =  try AVAudioPlayer(contentsOf: recordedAudioURL)
            recordPlaySlider.maximumValue = Float(audioPlayer?.duration ?? 0)
            
            self.audioPlayer?.delegate = self
            
            if timer == nil
            {
                timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
            }
        }
        catch
        {
            Helper.app.showAlert(title: Helper.Alerts.AudioFileError, message: String(describing: error.localizedDescription), vc: self)
        }
    }
    
    func stopTimer()
    {
        if timer != nil
        {
            timer?.invalidate()
            timer = nil
        }
    }
    
    
    func configureRecordindState(_ enabled: Bool)
    {
        if enabled
        {
            recordingLabel.text           = NSLocalizedString("Recording in Progress", comment: "RecordProgressField")
            recordDeleteButton.isEnabled = false
            recordDeleteButton.setTitleColor(UIColor.gray, for: .disabled)
        }
        else
        {
            recordingLabel.text           = NSLocalizedString("Voice Recorder", comment: "VoiceRecorderField")
            recordDeleteButton.isEnabled = true
        }
    }
    
    
    @IBAction func playPauseRecord(_ sender: UIButton)
    {
        if isPlaying
        {
            sender.setImage(imgPlay, for: .normal)
            audioPlayer?.pause()
            isPlaying = false
        }
        else
        {
            sender.setImage(imgPause, for: .normal)
            audioPlayer?.play()
            isPlaying = true
        }
    }
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        audioPlayer?.stop()
        recordPlaySlider.value = 0
        playPauseButton.setImage(imgPlay, for: .normal)
        isPlaying = false
    }
    
    
    @objc func updateSlider()
    {
        recordPlaySlider.value = Float(audioPlayer?.currentTime ?? 0)
    }
    
    
    @IBAction func changeRecordedAudioTime(_ sender: Any)
    {
        audioPlayer?.stop()
        audioPlayer?.currentTime = TimeInterval(recordPlaySlider.value)
        audioPlayer?.prepareToPlay()
        audioPlayer?.play()
    }
    
    
    @IBAction func recordDeleteButtonPressed(_ sender: UIButton)
    {
        let recordPath = recordedAudioURLtoSave?.absoluteString ?? ""
        recordPlayView.isHidden = true
        Helper.app.deleteFile(fileURL: recordPath, vc: self)
        recordedAudioURLtoSave = nil
    }
    
    @IBAction func cancelBtnWasPressed(_ sender: UIBarButtonItem)
    {
         dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelBtnPressed(_ sender: Any)
    {
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnWasPressed(_ sender: UIBarButtonItem)
    {
        self.save()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveBtnPressed(_ sender: Any)
    {
        self.save()
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func unwindToAddReminder(sender: UIStoryboardSegue)
    {
        if let sourceViewController = sender.source as? PhotoVC
        {
            photoPath = sourceViewController.photoPath
        }
    }

    
    func updateConfiguration ()
    {
        saveButton.title = NSLocalizedString("Update", comment: "updateField")
        saveUpdateBtn.setTitle(NSLocalizedString("Update", comment: "updateField"), for: .normal)
        titleTextField.text = reminder?.title
        bodyTextView.text = reminder?.body
        
        
        if #available(iOS 13.0, *)
        {
            bodyTextView.textColor = .label
        }
        else
        {
            // Fallback on earlier versions
            bodyTextView.textColor = UIColor.black
        }
        
        bodyTextView.font = UIFont.systemFont(ofSize: 18.0)
        
        timeButton.setTitle(Helper.app.formattedTime(date: (reminder?.reminder_date_time)!, for24Hours: for24Hours), for: .normal)
        dateButton.setTitle(Helper.app.formattedDate(date: (reminder?.reminder_date_time)!), for: .normal)
        
        self.selectedTime = Helper.app.formattedTimeForSelectedTime(date: (reminder?.reminder_date_time)!)
        self.selectedDate = Helper.app.formattedDateForSelectedDate(date: (reminder?.reminder_date_time)!)
        
        
        self.btnSelectRepeatInterval.setTitle(pickerData[(reminder?.repeat_interval.value)!], for: .normal)
        self.selectedRepeatIntervalRow = (reminder?.repeat_interval.value)!
        
        photoPath = (reminder?.photo_path)!
        
        if (reminder!.recorded_sound_path != "")
        {
            recordedAudioURLtoSave = URL(string: reminder!.recorded_sound_path)
            recordPlayView.isHidden = false
            prepareAudioPlayer(recordedAudioURL: recordedAudioURLtoSave!)
        }
        
        if (reminder!.reminderColor != "")
        {
            selectedReminderColor = (reminder?.reminderColor)!
            colorPickerBtn.backgroundColor = UIColor(hexString: selectedReminderColor)
        }
    }
    
    
    func save()
    {
        let newReminder = Reminder()
        let title = titleTextField.text!
        
        if bodyTextView.text == NSLocalizedString("Description :", comment: "DescriptionField")
        {
            bodyTextView.text = ""
        }
        
        let body = bodyTextView.text!
        let scheduleDate = Helper.app.formattedDateToSaveAsDate(dateString: selectedDate, timeString: selectedTime)
        let badge = NSNumber(value: UIApplication.shared.applicationIconBadgeNumber + 1)
        
        if (reminder != nil)
        {
            timestampReminderID = (reminder?.reminderID)!
        }
     
        newReminder.reminderID = timestampReminderID
        newReminder.title = title
        newReminder.body  = body
        newReminder.reminder_date_time = scheduleDate
        newReminder.repeat_interval.value = selectedRepeatIntervalRow
        newReminder.photo_path = photoPath
        newReminder.recorded_sound_path = recordedAudioURLtoSave?.absoluteString ?? ""
        newReminder.reminderColor = selectedReminderColor
        
        if(self.selectedRepeatIntervalRow == 0)
        {
            //normal alarm
            newReminder.alarm_status.value = 1
        }
        else
        {
            //repeatable alarm
            newReminder.alarm_status.value = 2
        }
        
        
        do
        {
            try realm.write
            {
                realm.add(newReminder, update: .all)
            }
        }
        catch
        {
            Helper.app.showAlert(title: Helper.Alerts.ReminderSaveError, message: String(describing: error.localizedDescription), vc: self)
        }
        
        // Request Notification Settings
        UNUserNotificationCenter.current().getNotificationSettings
            { (notificationSettings) in
                switch notificationSettings.authorizationStatus
                {
                case .notDetermined:
                    self.requestAuthorization(completionHandler:
                        { (success) in
                            guard success else { return }
                            
                            
                            if(self.selectedRepeatIntervalRow == 0)
                            {
                                // Schedule Normal Alarm
                                AlarmManagerHelper.app.scheduleNormalLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge)
                            }
                            else
                            {
                                //set Repeatable Alarm
                                AlarmManagerHelper.app.scheduleRepeatableLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge, repeatIntervalPosition: self.selectedRepeatIntervalRow )
                            }
                    })
                case .authorized:
                    // Schedule Local Notification
                    if(self.selectedRepeatIntervalRow == 0)
                    {
                        // Schedule Normal Alarm
                        AlarmManagerHelper.app.scheduleNormalLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge)
                    }
                    else
                    {
                        //set Repeatable Alarm
                        AlarmManagerHelper.app.scheduleRepeatableLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge, repeatIntervalPosition: self.selectedRepeatIntervalRow )
                    }
                case .denied:
                     Helper.app.showAlert(title: Helper.Alerts.NotificationsDisabledTitle, message: Helper.Alerts.NotificationsDisabledMessage, vc: self)
                case .provisional:
                    // Schedule Local Notification
                    if(self.selectedRepeatIntervalRow == 0)
                    {
                        // Schedule Normal Alarm
                        AlarmManagerHelper.app.scheduleNormalLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge)
                    }
                    else
                    {
                        //set Repeatable Alarm
                        AlarmManagerHelper.app.scheduleRepeatableLocalNotification(reminderIdentifier: self.timestampReminderID, reminderDate: scheduleDate, title: title, body: body, for24Hours: self.for24Hours, badge: badge, repeatIntervalPosition: self.selectedRepeatIntervalRow )
                    }
                case .ephemeral:
                    Helper.app.showAlert(title: "Fatal Error", message: String(describing: "Error error"), vc: self)
                @unknown default:
                    Helper.app.showAlert(title: "Fatal Error", message: String(describing: "Error error"), vc: self)
                }
        }
    }
    
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ())
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error
            {
                Helper.app.showAlert(title: Helper.Alerts.NotificationsAuthorizationFailed, message: String(describing: error.localizedDescription), vc: self)
            }
            
            completionHandler(success)
        }
    }
    
    
    
    //Reklam Icin Eklendi
    /*
    func addBannerViewToView(_ bannerView: GADBannerView)
    {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        if #available(iOS 11.0, *)
        {
            // In iOS 11, we need to constrain the view to the safe area.
            positionBannerViewFullWidthAtBottomOfSafeArea(bannerView)
        }
        else {
            // In lower iOS versions, safe area is not available so we use
            // bottom layout guide and view edges.
            positionBannerViewFullWidthAtBottomOfView(bannerView)
        }
    }
    
    // MARK: - view positioning
    @available (iOS 11, *)
    func positionBannerViewFullWidthAtBottomOfSafeArea(_ bannerView: UIView) {
        // Position the banner. Stick it to the bottom of the Safe Area.
        // Make it constrained to the edges of the safe area.
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
            guide.rightAnchor.constraint(equalTo: bannerView.rightAnchor),
            guide.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor)
            ])
    }
    
    func positionBannerViewFullWidthAtBottomOfView(_ bannerView: UIView) {
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .leading,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .leading,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .trailing,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .trailing,
                                              multiplier: 1,
                                              constant: 0))
        view.addConstraint(NSLayoutConstraint(item: bannerView,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: bottomLayoutGuide,
                                              attribute: .top,
                                              multiplier: 1,
                                              constant: 0))
    }*/
    
 
}
