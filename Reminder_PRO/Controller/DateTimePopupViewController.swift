//
//  DateTimePopupViewController.swift
//  Reminder
//
//  Created by Boss on 26.12.2018.
//  Copyright © 2018 Boss. All rights reserved.
//

import UIKit

class DateTimePopupViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var saveButton: UIButton!
    var showTimePicker: Bool = false
    var for24Hours: Bool = true
    var onSave: ((_ data1: String, _ data2: String) -> ())?
       
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if showTimePicker
        {
              titleLabel.text = NSLocalizedString("SELECT TIME", comment: "selectTime")
            saveButton.setTitle(NSLocalizedString("Save Time",   comment: "saveTime"), for: .normal)
            datePicker.datePickerMode = .time
            
            if for24Hours
            {
                // For 24 Hrs
                datePicker.locale = Locale(identifier: "en_GB")
            }
            else
            {
                // For 12 Hrs
                datePicker.locale = Locale(identifier: "en_US")
            }
        }
        else
        {
              titleLabel.text = NSLocalizedString("SELECT DATE", comment: "selectDate")
            saveButton.setTitle(NSLocalizedString("Save Date",   comment: "saveDate"), for: .normal)
        }
    }
    

    @IBAction func saveDate_TouchUpInside(_ sender: UIButton)
    {
        if showTimePicker
        {
            onSave?(Helper.app.formattedTime(date: datePicker.date, for24Hours: for24Hours), Helper.app.formattedTimeForSelectedTime(date: datePicker.date))
        }
        else
        {
            onSave?(Helper.app.formattedDate(date: datePicker.date), Helper.app.formattedDateForSelectedDate(date: datePicker.date))
        }
        
        dismiss(animated: true)
    }
    

}
